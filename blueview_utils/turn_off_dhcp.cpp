/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// #include "barebones_driver/barebones_driver.h"

#include <bvt_sdk.h>
#include <unistd.h>  // for sleep

#include <algorithm>  // for max
// for timing  the pings
// cpplint flags this include because Google has its own timing library.
#include <chrono>    // NOLINT[build/c++]
#include <iostream>  // for cout. Remove if we ROSify.
#include <set>
#include <string>
#include <vector>

// TODO: will need to make this driver robust to the sonar being plugged /
// unplugged / etc. If a read fails, move to "unconfigured" state, and try
// again.
int main(int argc, char** argv) {
  try {
    // This is the default IP
    std::string listen_ip = "192.168.1.45";
    BVTSDK::Sonar sonar;
    sonar.Open("NET", listen_ip);
    std::cout << "Opened sonar at: " << listen_ip << std::endl;

    BVTSDK::NetworkSettings network_settings = sonar.GetNetworkSettings();
    // First, print out current settings
    int address_mode = network_settings.GetAddressMode();
    bool dhcp_enabled = network_settings.GetDHCPServerEnabled();
    std::string ipv4_address = network_settings.GetIPv4Address();
    std::string subnet_mask = network_settings.GetSubnetMask();
    std::string multicast_address = network_settings.GetIPv4MulticastAddress();
    std::string gateway = network_settings.GetGateway();

    std::cout << "AddressMode: " << address_mode << std::endl;
    std::cout << "DHCPServerEnabled: " << dhcp_enabled << std::endl;
    std::cout << "IPv4Address: " << ipv4_address << std::endl;
    std::cout << "SubnetMask: " << subnet_mask << std::endl;
    std::cout << "MulticastAddress: " << multicast_address << std::endl;
    std::cout << "Gateway: " << gateway << std::endl;

    std::cout << "Toggle DHCPServerEnabled? (y/n)?";
    std::string input;
    std::cin >> input;
    if ("y" == input) {
      std::cout << "Toggling dhcp enabled! Setting to: " << !dhcp_enabled
                << std::endl;
      network_settings.SetDHCPServerEnabled(!dhcp_enabled);
      sonar.SetNetworkSettings(network_settings);
    }
  } catch (BVTSDK::SdkException& ex) {
    std::cout << "configuration failed! code " << ex.ReturnCode() << std::endl;
    std::cout << "Error name: " << ex.ErrorName() << std::endl;
    std::cout << "Error message: " << ex.ErrorMessage() << std::endl;
    std::cout << std::endl;
  }

  return 0;
}
