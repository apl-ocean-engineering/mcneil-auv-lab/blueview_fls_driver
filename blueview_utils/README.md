## blueview_utils

command-line scripts that do useful one-off things with the sonar

### query_params

I needed to confirm that the .son file contains metadata about sonar settings,
including: time varying gain, analog gain, source_level, and ping timestamps.

The ProViewer software doesn't seem to show show this, so I wrote a quick
utility to step through all pings in a .son file and output their relevant
metadata.


### turn_off_dhcp

For easy benchtop debugging, Teledyne configured the sonar to provide
a DHCP server, which allows a computer to be directly plugged into the
ethernet port on the deckbox.

However, when we're running on the robot, the sonar will be plugged
into a switch, and the other devices will have static IPs. So, we want
to turn off the DHCP server.

*with sonar connected directly*:
* `./turn_off_dhcp`; if "DHCPServerEnabled: 1", then select 'y'
* reboot sonar, plugged into switch
* ensure laptop has static ip. To set one temporarily:
  * `sudo ip addr add 192.168.1.99/24`
  * `sudo ip link set dev eth0 up`
* Rerun `./turn_off_dhcp`, confirm "DHCPServerEnabled: 0"
