/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <bvt_sdk.h>
#include <getopt.h>  // for getopt_long
#include <unistd.h>  // for sleep and getopt (short version and externs)

#include <algorithm>  // for max
// for timing  the pings
// cpplint flags this include because Google has its own timing library.
#include <chrono>    // NOLINT[build/c++]
#include <iostream>  // for cout. Remove if we ROSify.
#include <set>
#include <string>
#include <vector>

int main(int argc, char** argv) {
  static struct option long_options[] = {
      {"filename", required_argument, NULL, 'f'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0}};
  std::string optstring = "n:f:h";

  std::string usage =
      "query_params: Output metadata about pings in a Blueview .son file.\n"
      "USAGE: query_params -f filename\n"
      "  -n, --filename:  Filename to open.\n"
      "  -f, --head: Which head to use; corresponding to FOV.\n"
      "              Valid values will depend on the specific sonar.\n"
      "              For the M900-MKII, options are: M900-130, M900-90, "
      "M900-45 \n"
      "  -h, --help: Print this message and exit.\n";

  std::string filename = "";
  std::string requested_head = "";

  int option_index = 0;  // we don't use this, but still have to pass it in.
  int cc;
  while (true) {
    cc =
        getopt_long(argc, argv, optstring.c_str(), long_options, &option_index);
    if (cc == -1) {
      break;
    }
    switch (cc) {
      case 0: {
        std::cout << "Unexpcted return value of 0 from getopt. Ignoring. \n";
        break;
      }
      case '?': {
        // This indicates that a required option is missing.
        std::cout << "Option is missing required argument. Ignoring. \n";
        break;
      }
      case 'n': {
        filename = std::string(optarg);
        break;
      }
      case 'f': {
        requested_head = std::string(optarg);
        break;
      }
      case 'h': {
        std::cout << usage;
        return 0;
      }
      default: {
        std::cout << "Unhandled return value from getopt_long: " << cc << "\n";
      }
    }
  }

  std::cout << "Loading filename: " << filename << "\n";
  BVTSDK::Sonar file_sonar;
  file_sonar.Open("FILE", filename);

  try {
    float source_level = file_sonar.GetSourceLevel();
    std::cout << "Source level: " << source_level << "\n";
  } catch (const BVTSDK::SdkException& ex) {
    std::cout << "Failed to call GetSourceLevel(). \n";
    std::cout << " - Return code " << ex.ReturnCode() << std::endl;
    std::cout << " - Error name: " << ex.ErrorName() << std::endl;
    std::cout << " - Error message: " << ex.ErrorMessage() << std::endl;
  }

  try {
    bool use_local_clock = file_sonar.GetTimestampsUseLocalClock();
    std::cout << "TimestampsUseLocalClock: " << use_local_clock << "\n";
  } catch (const BVTSDK::SdkException& ex) {
    std::cout << "Failed to call GetTimestampsUseLocalClock(). \n";
    std::cout << " - Return code " << ex.ReturnCode() << std::endl;
    std::cout << " - Error name: " << ex.ErrorName() << std::endl;
    std::cout << " - Error message: " << ex.ErrorMessage() << std::endl;
  }

  std::cout << "Trying to read data from head: " << requested_head << "\n";
  std::vector<BVTSDK::Head> heads;
  int active_head_id = -1;
  int num_heads = file_sonar.GetHeadCount();
  for (int ii = 0; ii < num_heads; ii++) {
    auto head = file_sonar.GetHead(ii);
    heads.push_back(head);
  }
  if (heads.size() == 0) {
    std::cout << "  - ERROR: Sonar doesn't have any heads. Can't configure!\n";
    return 0;
  }
  for (auto head : heads) {
    if (0 == requested_head.compare(head.GetHeadName())) {
      active_head_id = head.GetHeadID();
      std::cout << "  - Found head " << requested_head << ", ID is "
                << active_head_id << "\n";
    }
  }
  if (active_head_id == -1) {
    std::cout << "  - WARNING: Could not find a match for head: "
              << requested_head << ". Defaulting to: " << heads[0].GetHeadName()
              << '\n';
    active_head_id = heads[0].GetHeadID();
  }
  BVTSDK::Head active_head = file_sonar.GetHead(active_head_id);

  float gain = active_head.GetGainAdjustment();
  std::cout << "GainAdjustment: " << gain << "\n";
  float tvg = active_head.GetTVGSlope();
  std::cout << "TVG Slope: " << tvg << "\n";

  try {
    for (int ii = 0; ii < 10; ii++) {
      BVTSDK::Ping ping = active_head.GetPing(-1);
      double timestamp = ping.GetTimestamp();
      int offset = ping.GetTimeZoneOffset();
      std::cout << "Ping # " << ii << " had timestamp: " << std::fixed
                << timestamp << ", with timezone offset: " << offset << "\n";
    }
  } catch (const BVTSDK::SdkException& ex) {
    std::cout << "Failed to call GetTimeIndexedPing(). \n";
    std::cout << " - Return code " << ex.ReturnCode() << std::endl;
    std::cout << " - Error name: " << ex.ErrorName() << std::endl;
    std::cout << " - Error message: " << ex.ErrorMessage() << std::endl;
  }
}
