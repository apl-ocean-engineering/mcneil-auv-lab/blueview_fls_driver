#! /usr/bin/env bash

# Script to build the barebones driver for emulation on the ODroid
arm-linux-gnueabihf-g++ -I/usr/local/include -c -o barebones_driver.o src/barebones_driver.cpp
arm-linux-gnueabihf-g++ barebones_driver.o  -lbvtsdk -o barebones_driver
patchelf --set-interpreter /usr/arm-linux-gnueabihf/lib/ld-linux-armhf.so.3 barebones_driver

# The resulting binary can be run using:
# LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/arm-linux-gnueabihf/lib; ./barebones_driver
