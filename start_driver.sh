#! /usr/bin/env bash

# script to start the blueview driver, since there are
# so many command line options

if [ $# != 1 ]; then
    echo "USAGE: ./start_driver.sh missionNNN";
    exit 1;
fi
mission=$1

# Collect data out to this range; maximum supported is 100m.
max_range=100
# This determines FOV. Options are M900-130, M900-90, M900-45
sonar_head="M900-130"
# Analog gain applied by receiver electronics (dB) [0-50 dB]
gain=25
# Time varying gain (dB/m) [0-10 dB/m]
tvg=0.0
# Transmit power, as fraction of max. (0.0-1.0 are valid values)
source_level=0.5
# How often to start a new log file
rollover_minutes=5

output_dir="/home/romulus/dive_logs"/${mission}
mkdir -p ${output_dir}
log_filepath=${output_dir}/blueview.log

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib:/usr/:/usr/arm-linux-gnueabihf/lib;

echo "./barebones_driver --directory ${output_dir} --log_rollover_time ${rollover_minutes} --max_range ${max_range} --head ${sonar_head} --gain ${gain} --tvg ${tvg} --source_level ${source_level} | tee --append ${log_filepath}"

 ./barebones_driver --directory ${output_dir} --log_rollover_time ${rollover_minutes} --max_range ${max_range} --head ${sonar_head} --gain ${gain} --tvg ${tvg} --source_level ${source_level} | tee --append ${log_filepath}
