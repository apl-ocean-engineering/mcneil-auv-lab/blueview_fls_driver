/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
// #include "barebones_driver/barebones_driver.h"

#include <bvt_sdk.h>
#include <getopt.h>  // for getopt_long
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>  // for sleep and getopt (short version and externs)

#include <algorithm>  // for max
// for timing  the pings
// cpplint flags this include because Google has its own timing library.
#include <chrono>    // NOLINT[build/c++]
#include <iomanip>   // for put_time
#include <iostream>  // for cout. Remove if we ROSify.
#include <set>
#include <sstream>  // for stringstream
#include <string>
#include <vector>

class BlueviewDriver {
 public:
  BlueviewDriver(std::string output_directory, double rollover_minutes,
                 std::string sonar_head, double max_range, double gain,
                 double tvg, double source_level, bool trigger_enabled,
                 int trigger_delay);
  ~BlueviewDriver() = default;

  // Main loop; if sonar is connected, ask it to ping. Otherwise, try to
  // connect.
  void run();

 private:
  // Configure sonar with max range and gain
  // Will eventually use the provided params.
  void configure();
  // grabs and saves single frame from the sonar
  void ping();
  // iterate through most (but not all) configurations and print them out
  void printSettings();
  // print details of exception to stdout
  // input msg is additional information from calling function
  void printSdkException(const std::string& msg,
                         const BVTSDK::SdkException& ex);
  // Start logging to new file
  void startNewOutputFile();

  // Sonar that we're reading data from
  BVTSDK::Sonar net_sonar_;
  // "Sonar" object managing the file that we're writing data to
  BVTSDK::Sonar file_sonar_;

  // IP of the network sonar
  std::string listen_ip_;

  // Whether to ping on external trigger
  bool trigger_enabled_;
  // If external trigger enabled, use this delay (in ms)
  int trigger_delay_;

  // Sonar will record data from [start_range_, stop_range_]
  // Must be within the bounds of min/max range.
  float start_range_;
  // Invalid values will be set to max range.
  float stop_range_;

  // User input for requested head
  std::string requested_head_;
  // Which sonar head to use
  int active_head_id_;

  // Requested analog gain
  double gain_;
  // Requested time varying gain
  double tvg_;
  // Requested source level
  double source_level_;

  // directory to put output files in. Filename will be:
  // "/directory/remus_yyyymmdd_hhmmss.son"
  std::string output_directory_;
  // Timepoint for last time the output file was created
  std::chrono::time_point<std::chrono::steady_clock> file_timepoint_;
  // Minutes before a new output fill is created
  double rollover_minutes_;

  // // Image Generator to use for real-time processing of the ping data
  // BVTSDK::ImageGenerator image_generator_;

  // Whether we are currently attached to a networked sonar
  bool is_configured_ = false;

  // All heads attached to the sonar
  std::vector<BVTSDK::Head> heads_;
};

BlueviewDriver::BlueviewDriver(std::string output_directory,
                               double rollover_minutes, std::string sonar_head,
                               double max_range, double gain, double tvg,
                               double source_level, bool trigger_enabled,
                               int trigger_delay) {
  // Will be "" if there was invalid or missing input to main()
  output_directory_ = output_directory;
  // This is the Blueview default
  listen_ip_ = "192.168.1.45";
  //
  rollover_minutes_ = rollover_minutes;

  // Trigger settings
  trigger_enabled_ = trigger_enabled;
  trigger_delay_ = trigger_delay;

  // Can't sanity-check this until connected to the sonar.
  active_head_id_ = -1;
  requested_head_ = sonar_head;

  // For now, don't support setting the start range; assume user wants data
  // from as close as possible to the sonar.
  start_range_ = 0;
  stop_range_ = max_range;

  // TODO: Are there any
  gain_ = gain;
  tvg_ = tvg;
  source_level_ = source_level;

  is_configured_ = false;
}

void BlueviewDriver::configure() {
  // Need to reset member variables because this may be called again after
  // sonar times out
  heads_.clear();
  active_head_id_ = -1;

  std::cout << "\nConfiguring sonar at IP: " << listen_ip_ << std::endl;
  try {
    try {
      net_sonar_.Open("NET", listen_ip_);
      std::cout << "  - Opened input sonar!" << std::endl;
    } catch (const BVTSDK::SdkException& ex) {
      // Fatal error for configuration; return now and try again on next loop.
      printSdkException("Unable to open sonar", ex);
      return;
    }

    // Setting sonar to always use computer's local clock time.
    // If `true`, will report timestamps that seem to correspond to `uptime`
    // If `false` will report what appears to be time since sonar booted.
    // Since neither works as expected, we manually set each ping's timestamp
    // to seconds since the epoch using std::chrono.
    // TODO: Figure out why it's not working on the ODroids.
    try {
      net_sonar_.SetTimestampsUseLocalClock(true);
    } catch (const BVTSDK::SdkException& ex) {
      // This isn't great, but shouldn't be a fatal error at least for now,
      // since we're overwriting the timestamps.
      printSdkException("Couldn't SetTimestampsUseLocalClock", ex);
    }

    ////////
    // First, figure out if input head label is valid; otherwise choose
    // default.

    // I'm surprised that GetHeadCount throws, since it could just return 0
    int num_heads = net_sonar_.GetHeadCount();
    for (int ii = 0; ii < num_heads; ii++) {
      auto head = net_sonar_.GetHead(ii);
      heads_.push_back(head);
    }
    if (heads_.size() == 0) {
      std::cout
          << "  - ERROR: Sonar doesn't have any heads. Can't configure!\n";
      return;
    }
    for (auto head : heads_) {
      if (0 == requested_head_.compare(head.GetHeadName())) {
        active_head_id_ = head.GetHeadID();
        std::cout << "  - Found head " << requested_head_ << ", ID is "
                  << active_head_id_ << "\n";
      }
    }
    if (active_head_id_ == -1) {
      std::cout << "  - WARNING: Could not find a match for head: "
                << requested_head_
                << ". Defaulting to: " << heads_[0].GetHeadName() << '\n';
      active_head_id_ = heads_[0].GetHeadID();
    }

    ////////
    // For the BSEE configuration, the sonar needs to ping based on
    // an input trigger line.
    if (trigger_delay_ < 0) {
      std::cout << "WARNING: Invalid trigger_delay (" << trigger_delay_
                << " ms). Disabling trigger. \n";
      trigger_enabled_ = false;
    }
    bool supports_external_hardware_trigger =
        net_sonar_.SupportsExternalHardwareTrigger();
    if (trigger_enabled_ && supports_external_hardware_trigger) {
      net_sonar_.EnableExternalHardwareTrigger(trigger_delay_);
    } else {
      net_sonar_.DisableExternalHardwareTrigger();
      if (trigger_enabled_) {
        std::cout << "WARNING: Requested external trigger, but hardware does "
                     "not support it!\n";
      }
    }

    ///////
    // Handle range adjustment for the selected head
    BVTSDK::Head active_head = net_sonar_.GetHead(active_head_id_);
    float min_range = active_head.GetMinimumRange();
    float max_range = active_head.GetMaximumRange();
    if (start_range_ < min_range) {
      // TODO: re-enable this warning if/when users can set start_range
      // std::cout << "Requested start range of " << start_range_
      //          << ", which is shorter than the minimum supported range of "
      //          << min_range << std::endl;
      start_range_ = min_range;
    }
    if (stop_range_ > max_range) {
      std::cout << "  - WARNING: Requested stop range of " << stop_range_
                << ", which is longer than the maximum supported range of "
                << max_range << std::endl;
      stop_range_ = max_range;
    } else if (stop_range_ < min_range) {
      std::cout << "  - WARNING: Invalid stop range of " << stop_range_
                << ". Defaulting to max range\n";
      stop_range_ = max_range;
    }
    std::cout << "  - setting range to " << start_range_ << " to "
              << stop_range_ << std::endl;
    active_head.SetRange(start_range_, stop_range_);

    /////
    // Handle setting source level
    bool supports_source_level_control =
        net_sonar_.SupportsSourceLevelControl();
    if (supports_source_level_control) {
      // This call may be unnecessary, since SetSourceLevel disables auto.
      net_sonar_.EnableAutoSourceLevel(false);
      if (source_level_ > 1.0) {
        std::cout << "  - WARNING: Requested invalid source level: "
                  << source_level_
                  << ". Should be fraction of maximum power, in range of "
                     "(0.0, 1.0). Defaulting to max. \n";
        source_level_ = 1.0;
      } else if (source_level_ < 0.0) {
        std::cout << "  - WARNING: Requested invalid source level: "
                  << source_level_ << ". Setting based on stop_range_.\n";
        // I did a little experimentation on the bench ... it was NOT constant
        // as a function of stop_range, but there was a clear trend.
        source_level_ = stop_range_ / max_range;
      }
      std::cout << "  - Setting source level to: " << source_level_ << "\n";
      net_sonar_.SetSourceLevel(source_level_);
    } else {
      std::cout << "  - Sonar does not support source level control.\n";
    }

    ///////
    // Handle setting analog gain
    // These limits match what the ProViewer sliders allowed.
    if (gain_ > 50.0) {
      std::cout << "  - WARNING: maximum gain is 50 dB. You input: " << gain_
                << ". Setting to max.\n";
      gain_ = 50.0;
    } else if (gain_ < 0.0) {
      std::cout << "  - WARNING: minimum gain is 0 dB. You input: " << gain_
                << " (or did not configure gain). Defaulting to minimum.\n";
      gain_ = 0.0;
    }
    active_head.SetGainAdjustment(gain_);

    ///////
    // Handle setting time vaying gain
    // These limits match what the ProViewer sliders allowed.
    if (tvg_ > 10.0) {
      std::cout
          << "  - WARNING: maximum time varying gain is 10 dB/m. You input: "
          << tvg_ << ". Setting to max.\n";
      tvg_ = 10.0;
    } else if (tvg_ < 0.0) {
      std::cout << "  - WARNING: minimum time varying gain is 0 dB. You input: "
                << tvg_
                << " (or did not configure gain). Defaulting to minimum.\n";
      tvg_ = 0.0;
    }
    active_head.SetTVGSlope(tvg_);

    // image_generator_.SetHead(active_head);

    // If this fails, that's fine, sonar is already configured.
    try {
      printSettings();
    } catch (const BVTSDK::SdkException& ex) {
      printSdkException("printSettings failed", ex);
    }
    is_configured_ = true;
  } catch (const BVTSDK::SdkException& ex) {
    printSdkException("Configuration failed", ex);
  }
  // Need to create file AFTER configuring the sonar so it'll (mostly)
  // have the right metadata. source_level is still a problem.
  if (is_configured_) {
    startNewOutputFile();
  }
}

void BlueviewDriver::startNewOutputFile() {
  auto t_chrono = std::chrono::system_clock::now();
  std::time_t t_time = std::chrono::system_clock::to_time_t(t_chrono);
  std::tm t_tm = *std::localtime(&t_time);
  std::stringstream ss;
  ss << std::put_time(&t_tm, "%Y%m%d_%H%M%S");
  std::string output_filename = "remus_" + ss.str() + ".son";
  std::string output_filepath;
  if (!output_directory_.empty()) {
    output_filepath = output_directory_ + "/" + output_filename;
  } else {
    output_filepath = output_filename;
  }
  std::cout << "Trying to open file at: " << output_filepath << "\n";
  try {
    file_sonar_.CreateFile(output_filepath, net_sonar_, "");
    std::cout << "  - Opened output file at: " << output_filepath << std::endl;
    file_timepoint_ = std::chrono::steady_clock::now();
  } catch (const BVTSDK::SdkException& ex) {
    // TODO: we will eventually be re-initialized if file_sonar_ is invalid,
    //    since ping() will fail, setting is_configured_=False
    printSdkException("Couldn't open output file.", ex);
  }
}

void BlueviewDriver::printSdkException(const std::string& msg,
                                       const BVTSDK::SdkException& ex) {
  auto tp = std::chrono::system_clock::now();
  auto ms_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(
      tp.time_since_epoch());
  double timestamp = ms_since_epoch.count() / 1000.0;
  std::cout << "[ " << std::fixed << timestamp
            << " ] BvtSdkException. Return code: " << ex.ReturnCode()
            << "; Error name: " << ex.ErrorName()
            << "; Error message: " << ex.ErrorMessage()
            << ". User message: " << msg << "\n";
}

void BlueviewDriver::printSettings() {
  std::cout << "\nSonar Settings:\n";
  std::string serial_number = net_sonar_.GetSerialNumber();
  std::string firmware_revision = net_sonar_.GetFirmwareRevision();
  std::string sonar_name = net_sonar_.GetSonarName();
  std::string model_name = net_sonar_.GetSonarModelName();
  std::string part_number = net_sonar_.GetPartNumber();
  int head_count = net_sonar_.GetHeadCount();
  float temperature = net_sonar_.GetTemperature();

  std::cout << "  - ModelName: " << sonar_name << std::endl;
  std::cout << "  - SonarModelName: " << model_name << std::endl;
  std::cout << "  - PartNumber: " << part_number << std::endl;
  std::cout << "  - SerialNumber: " << serial_number << std::endl;
  std::cout << "  - FirmwareRevision: " << firmware_revision << std::endl;
  std::cout << "  - HeadCount: " << head_count << std::endl;
  std::cout << "  - Temperature: " << temperature << std::endl;

  bool timestamps_use_local_clock = net_sonar_.GetTimestampsUseLocalClock();
  bool supports_multicast = net_sonar_.SupportsMulticast();

  std::cout << "  - TimestampsUseLocalClock: " << timestamps_use_local_clock
            << std::endl;
  std::cout << "  - SupportsMulticast: " << supports_multicast << std::endl;

  bool supports_dead_time_between_pings =
      net_sonar_.SupportsDeadTimeBetweenPings();
  std::cout << "  - SupportsDeadTimeBetweenPings: "
            << supports_dead_time_between_pings << std::endl;
  if (supports_dead_time_between_pings) {
    int dead_time_between_pings = net_sonar_.GetDeadTimeBetweenPings();
    std::cout << "  - DeadTimeBetweenPings: " << dead_time_between_pings
              << " ms\n";
  }

  bool supports_external_hardware_trigger =
      net_sonar_.SupportsExternalHardwareTrigger();
  std::cout << "  - SupportsExternalHardwareTrigger: "
            << supports_external_hardware_trigger << "\n";
  if (supports_external_hardware_trigger) {
    bool external_hardware_trigger_enabled =
        net_sonar_.GetExternalHardwareTriggerEnabled();
    std::cout << "  - ExternalHardwareTriggerEnabled: "
              << external_hardware_trigger_enabled << "\n";
    bool external_hardware_trigger_direction =
        net_sonar_.GetExternalHardwareTriggerDirection();
    std::cout << "  - ExternalHardwareTriggerDirection: is_input = "
              << external_hardware_trigger_direction << "\n";
    if (external_hardware_trigger_direction) {
      int external_hardware_trigger_delay_ms =
          net_sonar_.GetExternalHardwareTriggerInputDelay();
      std::cout << "  - ExternalHardwareTriggerInputDelay: delay_in_ms = "
                << external_hardware_trigger_delay_ms << "\n";
    }
  }

  bool supports_source_level_control = net_sonar_.SupportsSourceLevelControl();
  std::cout << "  - SupportsSourceLevelControl: "
            << supports_source_level_control << std::endl;
  if (supports_source_level_control) {
    bool auto_source_level_enabled = net_sonar_.IsAutoSourceLevelEnabled();
    float source_level = net_sonar_.GetSourceLevel();
    std::cout << "  - AutoSourceLevelEnabled: " << auto_source_level_enabled
              << "\n";
    std::cout << "  - SourceLevel: " << source_level << "\n";
  }
  // Not bothering to record info about: network settings, nav data,
  // event marks, target tracking

  std::cout << "\nThe active head is: " << active_head_id_ << "\n";

  for (auto& head : heads_) {
    int head_id = head.GetHeadID();
    std::string head_name = head.GetHeadName();
    std::cout << "\nHead #" << head_id << " is: " << head_name << std::endl;
    float min_range = head.GetMinimumRange();
    float max_range = head.GetMaximumRange();
    float start_range = head.GetStartRange();
    float stop_range = head.GetStopRange();
    std::cout << "  - Available range: " << min_range << " to " << max_range
              << " m" << std::endl;
    std::cout << "  - Configured range: " << start_range << " to " << stop_range
              << " m" << std::endl;

    float center_freq = head.GetCenterFreq();
    int transducer_count = head.GetTransducerCount();
    bool dynamic_power_management = head.GetDynamicPowerManagement();
    float gain_adjustment = head.GetGainAdjustment();
    float tvg_slope = head.GetTVGSlope();
    // Alternate ping mode provides better imagery for wide FOV, at the
    // cost of slower ping rate. Defaults to "on" for 130 deg FOV,
    // and "off" for 90 and 45 deg, though the docs suggest it is also
    // worthwhile at 90.
    bool supports_alternate_ping_mode = head.SupportsAlternatePingMode();

    std::cout << "  - CenterFreq: " << center_freq << std::endl;
    std::cout << "  - TransducerCount: " << transducer_count << std::endl;
    std::cout << "  - DynamicPowerManagement: " << dynamic_power_management
              << std::endl;
    std::cout << "  - GainAdjustment: " << gain_adjustment << std::endl;
    std::cout << "  - TVGSlope: " << tvg_slope << std::endl;
    std::cout << "  - SupportsAlternatePingMode: "
              << supports_alternate_ping_mode << std::endl;
    if (supports_alternate_ping_mode) {
      bool alternate_ping_mode_enabled = head.GetAlternatePingModeEnabled();
      std::cout << "  - AlternatePingModeEnabled: "
                << alternate_ping_mode_enabled << "\n";
    }
    // SO far as I can tell, this doesn't actually matter -- sound speed
    // can be adjusted after-the-fact as part of the image processing.
    int fluid_type = head.GetFluidType();
    double sound_speed = head.GetSoundSpeed();  // configured, NOT measured
    std::cout << "  - FluidType: " << fluid_type << "\n";
    std::cout << "  - SoundSpeed: " << sound_speed << "\n";
    double mru_time_offset = head.GetMRUTimeOffset();
    double ping_interval = head.GetPingInterval();
    std::cout << "  - MRU TimeOffset: " << mru_time_offset << "\n";
    std::cout << "  - PingInterval: " << ping_interval << "\n";

    // Not bothering to query any of the parameters involved in pan/tilt,
    // orientation, or mounting type, since we don't care.
  }
}

void BlueviewDriver::ping() {
  // Using steady_clock for timing, system_clock for timestamps
  auto t0 = std::chrono::steady_clock::now();
  auto tp = std::chrono::system_clock::now();

  // This will throw if the sonar is disconnected.
  // We wrap the whole call to ping, rather than handling it within the
  // function.
  BVTSDK::Ping new_ping = heads_[active_head_id_].GetPing(-1);
  auto ms_since_epoch = std::chrono::duration_cast<std::chrono::milliseconds>(
      tp.time_since_epoch());
  double timestamp = ms_since_epoch.count() / 1000.0;
  new_ping.SetTimestamp(timestamp);

  auto t1 = std::chrono::steady_clock::now();
  BVTSDK::Head out_head = file_sonar_.GetHead(active_head_id_);
  out_head.PutPing(new_ping);
  auto t2 = std::chrono::steady_clock::now();

  // // Making an image is too slow to do in real-time on the ODroids.
  // BVTSDK::MagImage img = image_generator_.GetImageRTheta(new_ping);
  // int image_width = img.GetWidth();
  // int image_height = img.GetHeight();
  // std::cout << "Got image! width = " << image_width
  //           << ", height = " << image_height << std::endl;

  auto dt1 =
      std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
  auto dt2 =
      std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
  // std::cout << std::fixed << timestamp << ": "
  //           << dt1 << " ms to get ping, "
  //           << dt2 << " ms to write ping\n";
}

// I thought about trying to check IsConnected(), but that wound up not
// being useful. It returns true long past when GetPing() is timing out,
// so we might as well start trying to re-connect immediately.
// When IsConnected returns true, GetPing reports NET_NOT_CONNECTED,
// rather than waiting for timeouts.
void BlueviewDriver::run() {
  while (true) {
    if (!is_configured_) {
      // Want to make sure we don't try to create two files with same name,
      // and our time utility formats down to the second.
      sleep(5);
      configure();
    } else {
      // Check if we need a new file
      auto tt = std::chrono::steady_clock::now();
      auto dt = tt - file_timepoint_;
      auto dt_seconds =
          std::chrono::duration_cast<std::chrono::seconds>(dt).count();
      if (rollover_minutes_ > 0 && dt_seconds >= 60 * rollover_minutes_) {
        std::cout << "Opening new file after " << dt_seconds << " seconds\n";
        startNewOutputFile();
      }

      // If not being externally triggered, get the next ping
      try {
        ping();
        std::cout << dt_seconds << " Pinging!" << std::endl;
      } catch (const BVTSDK::SdkException& ex) {
        printSdkException("Failed to ping", ex);
        if (trigger_enabled_ && ex.ReturnCode() == BVT_TIMEOUT) {
          // If trigger is enabled but inactive, we expect this to faile
          // with a timeout, so no need to reconfigure.
          continue;
        } else {
          is_configured_ = false;
        }
      }
    }
  }
}

int main(int argc, char** argv) {
  static struct option long_options[] = {
      {"max_range", required_argument, NULL, 'r'},
      {"head", required_argument, NULL, 'f'},
      {"gain", required_argument, NULL, 'g'},
      {"tvg", required_argument, NULL, 't'},
      {"source_level", required_argument, NULL, 'l'},
      {"directory", required_argument, NULL, 'd'},
      {"log_rollover_time", required_argument, NULL, 'm'},
      {"trigger_delay", required_argument, NULL, 's'},
      {"help", no_argument, NULL, 'h'},
      {0, 0, 0, 0}};
  std::string optstring = "r:f:g:t:l:d:m:s:h";

  std::string usage =
      "barebones_driver: A simple-as-possible driver for a Blueview imaging "
      "sonar. \n"
      "USAGE : barebones_driver -d directory -r max_range -h head -g gain -t "
      "time_varying_gain -l source_level\n"
      "  -d, --directory:  Directory where logs will be saved.\n"
      "  -m, --rollover_time:  Time (in minutes) after which a new log "
      "                        file will be created. Otherwise, sonar files "
      "                        may become unmanageably large. \n"
      "  -r, --max_range:  Maximum range for each ping. \n"
      "                    If not set, defaults to sonar's max range. (m)\n"
      "  -f, --head: Which head to use; corresponding to FOV.\n"
      "              Valid values will depend on the specific sonar.\n"
      "              For the M900-MKII, options are: M900-130, M900-90, "
      "              M900-45.\n"
      "  -s, --trigger_delay: If set, enable external trigger and set \n"
      "                       the delay to input value. (delay in ms) \n"
      // These descriptions are taken from the ProViewer manual
      "  -g, --gain: Set 'base analog gain on the receiver amplifiers' (dB) "
      "\n"
      "  -t, --tvg: Set 'time variable gain levels on the receiver "
      "amplifiers'. "
      "(dB/m) \n"
      "  -l, --source_level: 'transmit power level applied at the sonar'.\n"
      "                       Ratio of desired value to max (0.0 to 1.0)\n"
      "  -h, --help: Print this message and exit.\n";

  // Parameters that we'll be parsing from the command line.
  // The parsing logic here only checks that they can be converted to the
  // expected type; validity checking will happen in the class constructor.
  double max_range = -1;
  std::string sonar_head = "none";
  std::string output_directory = "";
  double gain = -1;
  double tvg = -1;
  double source_level = -1;
  double rollover_minutes = -1;
  bool trigger_enabled = false;
  int trigger_delay = -1;

  int option_index = 0;  // we don't use this, but still have to pass it in.
  int cc;
  while (true) {
    cc =
        getopt_long(argc, argv, optstring.c_str(), long_options, &option_index);
    if (cc == -1) {
      std::cout << "BlueView driver: Done processing cmdline args! \n";
      break;
    }
    switch (cc) {
      case 0: {
        // This is used to indicate that `flag` has been set and that the flag
        // var is pointing to a variable. We're not using this for any of our
        // arguments.
        std::cout << "Unexpcted return value of 0 from getopt. Ignoring. \n";
        break;
      }
      case '?': {
        // This indicates that a required option is missing.
        std::cout << "Option is missing required argument. Ignoring. \n";
        break;
      }
      case 'r': {
        std::string range_str = std::string(optarg);
        try {
          max_range = std::stof(range_str);
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for max_range: " << range_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 'f': {
        sonar_head = std::string(optarg);
        break;
      }
      case 'd': {
        output_directory = std::string(optarg);
        break;
      }
      case 'g': {
        std::string gain_str = std::string(optarg);
        try {
          gain = std::stof(gain_str);
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for gain: " << gain_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 'm': {
        std::string rollover_str = std::string(optarg);
        try {
          rollover_minutes = std::stof(rollover_str);
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for rollover time: " << rollover_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 't': {
        std::string tvg_str = std::string(optarg);
        try {
          tvg = std::stof(tvg_str);
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for max_range: " << tvg_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 'l': {
        std::string source_level_str = std::string(optarg);
        try {
          source_level = std::stof(source_level_str);
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for max_range: " << source_level_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 's': {
        std::string trigger_delay_str = std::string(optarg);
        try {
          trigger_delay = std::stoi(trigger_delay_str);
          trigger_enabled = true;
        } catch (const std::invalid_argument& ex) {
          std::cout << "Invalid value for trigger_delay: " << trigger_delay_str
                    << "Raised exception: " << ex.what() << '\n';
        }
        break;
      }
      case 'h': {
        std::cout << usage;
        return 0;
      }
      default: {
        std::cout << "Unhandled return value from getopt_long: " << cc << "\n";
      }
    }
  }

  std::cout << "You requested arguments: \n";
  std::cout << "  * output_directory = " << output_directory << " \n";
  std::cout << "  * log_rollover_time = " << rollover_minutes << " (mins)\n";
  std::cout << "  * max_range = " << max_range << " (m) \n";
  std::cout << "  * sonar_head = " << sonar_head << '\n';
  std::cout << "  * gain = " << gain << " (dB) \n";
  std::cout << "  * tvg = " << tvg << " (dB/m) \n";
  std::cout << "  * source_level = " << source_level << "\n";
  if (trigger_enabled) {
    std::cout << "  * trigger_delay = " << trigger_delay << "\n";
  } else {
    std::cout << "  * No external trigger. \n";
  }

  // Test whether output directory is valid! If not, use cwd
  struct stat info;
  if (stat(output_directory.c_str(), &info) != 0) {
    std::cout << "Can't access path (" << output_directory
              << "); defaulting to .\n";
    output_directory = "";
  } else if (info.st_mode & S_IFDIR) {
    // we're  happy
  } else {
    std::cout << "Requested directory " << output_directory
              << ", which isn't a directory. Defaulting to .\n";
    output_directory = "";
  }

  BlueviewDriver bv(output_directory, rollover_minutes, sonar_head, max_range,
                    gain, tvg, source_level, trigger_enabled, trigger_delay);
  bv.run();
  return 0;
}
